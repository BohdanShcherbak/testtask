﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProductRepository : IProductRepository
    {

        private TestContext context;
        public ProductRepository(TestContext ctx)
        {
            context = ctx;
        }
        public void AddProduct(Product product)
        {
            context.Products.Add(product);
        }
        public void UpdateProduct(Product product)
        {
            context.Entry(product).State = EntityState.Modified;
        }
        public Product GetProductById(int ProductId)
        {
            return context.Products.Find(ProductId);
        }
        public void DeleteProduct(int Id)
        {
            var getProduct = context.Products.Find(Id);
            context.Products.Remove(getProduct);
        }

        public IQueryable GetAllProducts()
        {
            var data = context.Products.Select(s => new { ProductId = s.ProductId, Title = s.Title, Price = s.Price });
            return data;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        //quries
        public void TotalCount(int id)
        {
            var count = context.WareHouses.Where(s => s.ProductId == id).Count();
        }
        public void TotalCost(int id)
        {
           // var cost = context.WareHouses.Where(s => s.ProductId == id).Count() * context.Products.(s => s.Price);
        }
    }
}
