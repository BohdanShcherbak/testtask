namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "dbo.WareHouses",
                c => new
                    {
                        WareHouseId = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        AddingTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.WareHouseId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WareHouses", "ProductId", "dbo.Products");
            DropIndex("dbo.WareHouses", new[] { "ProductId" });
            DropTable("dbo.WareHouses");
            DropTable("dbo.Products");
        }
    }
}
