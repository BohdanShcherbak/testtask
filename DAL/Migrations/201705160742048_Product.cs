namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Product : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Products", "Title", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "Title" });
        }
    }
}
