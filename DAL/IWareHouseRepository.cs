﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IWareHouseRepository
    {
        IQueryable GetAllWareHouses();
        Warehouse GetWareHouseById(int WareHouseId);
        void DeleteWareHouse(int Id);
        void AddWareHouse(Warehouse warehouse);
        void UpdateWareHouse(Warehouse warehouse);
        void Save();
    }
}
