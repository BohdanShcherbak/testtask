﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class WareHouseRepository : IWareHouseRepository
    {
        private TestContext context;
        public WareHouseRepository(TestContext ctx)
        {
            context = ctx;
        }
        public void AddWareHouse(Warehouse warehouses)
        {
            warehouses.AddingTime = DateTime.Now;
            context.WareHouses.Add(warehouses);
        }
        public void UpdateWareHouse(Warehouse warehouses)
        {
            context.Entry(warehouses).State = EntityState.Modified;
        }
        public Warehouse GetWareHouseById(int WareHouseId)
        {
            return context.WareHouses.Find(WareHouseId);
        }
        public void DeleteWareHouse(int Id)
        {
            var getProduct = context.WareHouses.Find(Id);
            context.WareHouses.Remove(getProduct);
        }
        public IQueryable GetAllWareHouses()
        {
            var data = context.WareHouses.Select(s => new { WareHouseId = s.WareHouseId, ProductId = s.ProductId, AddingTime = s.AddingTime });
            return data;
        }


        public void Save()
        {
            context.SaveChanges();
        }

    }
}

