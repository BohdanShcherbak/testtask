﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IProductRepository
    {
        IQueryable GetAllProducts();
        Product GetProductById(int ProductId);
        void DeleteProduct(int Id);
        void AddProduct(Product product);
        void UpdateProduct(Product product);
        void Save();
    }
}
