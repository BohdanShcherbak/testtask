namespace DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class Product
    {
        public Product()
        {
            WareHouses = new HashSet<Warehouse>();
        }
        [DataMember]
        [Key]
        public int ProductId { get; set; }
        [DataMember]
        [Required]
        [StringLength(50, ErrorMessage ="Max length - 50 ")]
        [Index(IsUnique =true)]
        public string Title { get; set; }
        [DataMember]
        [Range(0, 10000, ErrorMessage ="Max value -10000")]
        public float Price { get; set; }

 
        public virtual ICollection<Warehouse> WareHouses { get; set; }
    }
}
