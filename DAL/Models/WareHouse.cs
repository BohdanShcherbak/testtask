namespace DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    [DataContract]
    [Table("WareHouses")]
    public partial class Warehouse
    {
        [DataMember]
        public int WareHouseId { get; set; }
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public DateTime AddingTime { get; set; }


        public virtual Product Product { get; set; }
    }
}
