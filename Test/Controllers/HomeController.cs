﻿using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Test.Models;
using Test.DTO;

namespace Test.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IProductRepository productRepository;
        Mapper mp = new Mapper();
        public HomeController()
        {
            productRepository = new ProductRepository(new TestContext());
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetAllProducts()
        {
            var data = mp.ListProduct();
            return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }



        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult CreateProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    productRepository.AddProduct(product);
                    productRepository.Save();

                    return Json(new { redirectUrl = Url.Action("Index", "Home") });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Error" + ex;
                }
            }
            return Json(new
            {
                errors = ModelState.Keys.SelectMany(i => ModelState[i].Errors).Select(m => m.ErrorMessage).ToArray()
            });
        }

        [HttpGet]
        public JsonResult GetProductById(int ProductId)
        {
            var product = productRepository.GetProductById(ProductId);

            var dt = mp.MappProduct(product);
            return new JsonResult() { Data = dt, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult GetProductSection(int ProductId)
        {
            TestContext context = new TestContext();
            var getProd = productRepository.GetProductById(ProductId);
            var count = context.WareHouses.Where(s => s.ProductId == ProductId).Count();
            var total = context.WareHouses.Where(s => s.ProductId == ProductId).Sum(s => s.Product.Price);

            SectionViewModel product = new SectionViewModel()
            {
                Price = getProd.Price,
                Title = getProd.Title,
                Count = count,
                Total = total
            };

            return new JsonResult() { Data = product, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string EditProduct(Product products)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    productRepository.UpdateProduct(products);
                    productRepository.Save();

                    return "Data successfully added";
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Error" + ex.Message);
                }
            }
            
            return "Что-то пошло не так!";
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public string DeleteProduct(int ProductId)
        {
            if (ModelState.IsValid)
            {
                productRepository.DeleteProduct(ProductId);
                productRepository.Save();
                return "Данные удалены!";
            }
            else
            {
                ModelState.AddModelError("", "Error");
                return "Что-то пошло не так!";
            }
        }





        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public string GetData()
        {
            return JsonConvert.SerializeObject(productRepository.GetAllProducts());
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}