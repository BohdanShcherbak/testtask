﻿using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Test.Models;
using Test.DTO;

namespace Test.Controllers
{
    [Authorize]
    public class WareHouseController : Controller
    {
        private IWareHouseRepository wareHoueRepository;
        Mapper mp = new Mapper();
        // GET: WareHouse
        public WareHouseController()
        {
            wareHoueRepository = new WareHouseRepository(new TestContext());
        }
        [HttpGet]
        public JsonResult GetAllWareHouses()
        {
            var data = mp.ListWareHouse().ToList();
            return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public JsonResult GetProducts()
        {
            var data = mp.ListProduct();
            return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public string CreateWareHouse(Warehouse warehouse)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    wareHoueRepository.AddWareHouse(warehouse);
                    wareHoueRepository.Save();
                    return "Данные добавлены!";
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Error"+ ex.Message);
                }
            }

            return "Что-то пошло не так!";

        }

        [HttpGet]
        public JsonResult GetWareHouseById(int WareHouseId)
        {
            var warehouse1 = wareHoueRepository.GetWareHouseById(WareHouseId);

            WarehouseViewModel warehouseViewModel = new WarehouseViewModel();
            warehouseViewModel.AddingTime = warehouse1.AddingTime;
            warehouseViewModel.ProductId = warehouse1.ProductId;
            warehouseViewModel.WareHouseId = warehouse1.WareHouseId;

            return new JsonResult() { Data = warehouseViewModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public string EditWareHouse(Warehouse warehouses)
        {
            wareHoueRepository.UpdateWareHouse(warehouses);
            wareHoueRepository.Save();

            return "Data successfully added";
        }

        public string DeleteWareHouse(int WareHouseId)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    wareHoueRepository.DeleteWareHouse(WareHouseId);
                    wareHoueRepository.Save();
                    return "Данные удалены!";
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Error" + ex.Message);
                }
            }

            return "Что-то пошло не так!";
        }

        public string GetData()
        {
            return JsonConvert.SerializeObject(wareHoueRepository.GetAllWareHouses());
        }
      
    }
}
