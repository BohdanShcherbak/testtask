﻿using DAL.Models;
using FastMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models;

namespace Test.DTO
{
    public class Mapper
    {
        TestContext context = new TestContext();

        public ProductViewModel MappProduct(Product product)
        {

            ProductViewModel productDto = TypeAdapter.Adapt<Product, ProductViewModel>(product);

            return productDto;
        }

        public WarehouseViewModel MappWareHouse(Warehouse warehouse)
        {

            WarehouseViewModel productDto = TypeAdapter.Adapt<Warehouse, WarehouseViewModel>(warehouse);

            return productDto;
        }


        public IQueryable<ProductViewModel> ListProduct()
        {
            var products = context.Products.Project().To<ProductViewModel>().ToList();

            var productList = context.Products.Select(s => new ProductViewModel()
            {
                ProductId = s.ProductId,
                Price = s.Price,
                Title = s.Title,
                Count = context.WareHouses.Where(a => a.ProductId == s.ProductId).Count()
            });
            return productList;
        }
        public IQueryable<WarehouseViewModel> ListWareHouse()
        {
            var warehouses = context.WareHouses.Project().To<WarehouseViewModel>().ToList();

            var productList = context.WareHouses.Select(s => new WarehouseViewModel()
            {
                WareHouseId = s.WareHouseId,
                ProductId = s.ProductId,
                AddingTime = s.AddingTime,
                FromatterAddinTime = s.AddingTime.ToString()
                
            });
            return productList;
        }

    }
}