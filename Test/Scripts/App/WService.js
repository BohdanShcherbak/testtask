﻿myapp.service('crud', function ($http) {

    this.getAllWareHouses = function () {
        return $http.get("/WareHouse/GetAllWareHouses");
    }

    this.getWareHouseId = function (WareHouseId) {
        return $http.get("/WareHouse/GetWareHouseById?WareHouseId=" + WareHouseId);
    }

    this.saveWareHouse = function (WareHouses) {
        var save = $http({
            method: 'POST',
            url: '/WareHouse/CreateWareHouse/',
            data: WareHouses
        });
        return save;
    }

    this.editWareHouse = function (WareHouses) {
        var save = $http({
            method: 'POST',
            url: '/WareHouse/EditWareHouse/',
            data: WareHouses
        });
        return save;
    }

    this.deleteWareHouse = function (WareHouseId) {
        var deleterecord = $http({
            method: 'POST',
            url: "/WareHouse/DeleteWareHouse?WareHouseId=" + WareHouseId
        });
        return deleterecord;
    }

    this.getProducts = function () {
        return $http.get("/WareHouse/GetProducts");
    }

});