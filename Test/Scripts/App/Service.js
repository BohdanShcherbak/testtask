﻿myapp.service('crudservice', function ($http) {

    this.getAllProducts = function () {
        return $http.get("/Home/GetAllProducts");
    }

    this.getProductId = function (ProductId) {
        return $http.get("/Home/GetProductById?ProductId=" + ProductId);
    }

    this.saveProduct = function (Products) {
        var save = $http({
            method: 'POST',
            url: '/Home/CreateProduct/',
            data: Products
        });
        return save;
    }

    this.editProduct = function (Products) {
        var save = $http({
            method: 'POST',
            url: '/Home/EditProduct/',
            data: Products
        });
        return save;
    }

    this.deleteProduct = function (ProductId) {
        var deleterecord = $http({
            method: 'POST',
            url: "/Home/DeleteProduct?ProductId=" + ProductId
        });
        return deleterecord;
    }

    this.getProductSection = function (ProductId) {
        return $http.get("/Home/GetProductSection?ProductId=" + ProductId);
    }

});