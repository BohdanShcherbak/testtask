﻿

myapp.controller('mc', function ($scope, crud, ngDialog) {

    var WareHouses = crud.getAllWareHouses();
    $scope.selectedProduct;
    $scope.selectionChanged = function (rowChanged) {
        if (rowChanged.isSelected) {

            alert("Information about WareHouse");
        }
    };

    //$scope.data = function () {
    //    var Prod = crud.getProducts();
    //    Prod.then(function (data) {
    //        $scope.products = data.data;
    //    },
    //        function () {
    //            console("Oops..", "Error occured while loading", "error");
    //        });
    //};

    loadProd();
    function loadProd() {
        var prod = crud.getProducts();
        prod.then(function (data) {
            $scope.products = data.data;
        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }




    $scope.gridOptions = {
        enableCellEditOnFocus: true,
        enableColumnResizing: true,
        enableFiltering: false,
        enableGridMenu: true,
        showGridFooter: false,
        showColumnFooter: true,
        fastWatch: false,
        rowIdentity: getRowId,
        getRowIdentity: getRowId,
        enablePaginationControls: true,

        paginationPageSizes: [10, 25, 50, 100],
        paginationPageSize: 10,
        importerDataAddCallback: function importerDataAddCallback(grid, newObjects) {
            $scope.myData = $scope.data.concat(newObjects);
        },
        columnDefs: [
          
            { field: 'ProductId' },
            { field: 'FromatterAddinTime' },
            {
                field: 'Do', enableFiltering: false, enableSort: false,
                cellTemplate: ' <a href="" ng-click="grid.appScope.openDeleteDialog(); grid.appScope.getWareHouseById(row.entity)" class="btn btn-danger">Delete</a>'
            }
        ],
        onRegisterApi: function onRegisterApi(registeredApi) {
            $scope.gridApi = registeredApi;
            //.selection.on.rowSelectionChanged($scope, $scope.selectionChanged);
           // $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
        }
    };
    //show all Warehouses
    $scope.gridOptions.multiSelect = false;

    function getRowId(row) {

        return row.WareHouseId;

    }
    loadWareHouses();
    function loadWareHouses() {
        var WareHouses = crud.getAllWareHouses();
        WareHouses.then(function (data) {
            
            $scope.WareHouses = data.data;
            $scope.gridOptions.data = $scope.WareHouses;
            
        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }

    /////////////////////////////////////////////////////////////

    $scope.WareHouse = { ProductId: $scope.ProductId }; 
    //open create dialog
    $scope.showAlertDetails = function () {
        ngDialog.openConfirm({
            template: 'addWarh',
            controller: 'mc',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByNavigation: true,
        }).then(function (WareHouse) {
            var saveWareHouses = crud.saveWareHouse(WareHouse);
            saveWareHouses.then(function (d) {
                loadWareHouses();
            });
        });
    }

    //get by id
    $scope.getWareHouseById = function (WareHouse) {
        var singlerecord = crud.getWareHouseId(WareHouse.WareHouseId);
        singlerecord.then(function (d) {
            var record = d.data;
            $scope.WareHouseId = record.WareHouseId;
            $scope.ProductId = record.ProductId;
            $scope.FromatterAddinTime = record.AddingTime;
        },
            function () {
                console("Oops...", "Error occured while getting record", "error");
            });
    }

    $scope.updateWareHouse = function () {
        var WareHouse = {
            WareHouseId: $scope.WareHouseId,
            ProductId: $scope.ProductId,
            FromatterAddinTime: $scope.AddingTime
        };
        var updaterecords = crud.editWareHouse(WareHouse);
        updaterecords.then(function (d) {
            $scope.closeAll();
            //alert('Запись успешно отредактирована!');
        },
            function () {
                console("Opps...", "Error occured while updating", "error");
            });
    }

    // edit dialog
    $scope.openEditDialog = function () {
        ngDialog.open({
            template: 'getWarh',
            controller: 'mc',
            className: 'ngdialog-theme-default',
            scope: $scope,
        });
    }


    // edit dialog
    //$scope.openDeleteDialog = function () {
    //    ngDialog.open({
    //        template: 'delWarh',
    //        controller: 'mc',
    //        className: 'ngdialog-theme-default',
    //        scope: $scope,
    //    });
    //}

    //close dialog window
    $scope.closeAll = function () {
        ngDialog.closeAll();
    };

    //add new WareHouse
    $scope.createWareHouse = function () {
        var WareHouse = {
            WareHouseId: $scope.WareHouseId,
            ProductId: $scope.ProductId,
     
        };


        var savewarehouse = crud.saveWareHouse(WareHouse);
        savewarehouse.then(function (d) {
            // $scope.ProductId = d.data.ProductId;
            loadWareHouses();
            $scope.closeAll();

          
            //console("Reord inserted successfully");
        });

    }


    $scope.deleteWareHouse = function (WareHouse) {
        var deleterecord = crud.deleteWareHouse($scope.WareHouseId);
        deleterecord.then(function (d) {
            var WareHouse = {
              WareHouseId: '',
                ProductId: '',
                FromatterAddinTime: ''
            };
          //  $scope.WareHouse;
            loadWareHouses();


        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }

    $scope.openDeleteDialog = function () {
        ngDialog.openConfirm({
            template: 'delWarh',
            controller: 'mc',
            className: 'ngdialog-theme-default',
            scope: $scope,

            closeByNavigation: true,
        }).then(function (WareHouse) {
            var deleterecord = crud.deleteWareHouse($scope.WareHouseId);
            deleterecord.then(function (d) {
               
                loadWareHouses();
            },
                function () {
                    console("Oops..", "Error occured while loading", "error");
                });
        });
    }




    //////////////////////////////////////////////////////////////

});