﻿myapp.controller('crudcontroller', function ($scope, crudservice, ngDialog, $interval, uiGridConstants) {
    var Products = crudservice.getAllProducts();
    $scope.selectionChanged = function (rowChanged) {
        if (rowChanged.isSelected) {
            $scope.getProductById($scope.ProductId)
            //alert("Information about Product");
        }
    };

    $scope.Products;
    $scope.Product = { }; 
    $scope.gridOptions = {
        enableCellEditOnFocus: true,
        enableColumnResizing: true,
        enableFiltering: false,
        enableGridMenu: true,
        showGridFooter: false,
        showColumnFooter: true,
        fastWatch: false,
        rowIdentity: getRowId,
        getRowIdentity: getRowId,
        enablePaginationControls: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        enableColumnMenus: false,
        enableRowHashing: false,
        onRegisterApi: function (gridApi) { $scope.gridApi = gridApi; },
        rowTemplate: "<div ng-dblclick=\"grid.appScope.openSectionDialog(); grid.appScope.getSection(row.entity)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>",
        paginationPageSizes: [10, 25, 50, 100],
        paginationPageSize: 10,

        importerDataAddCallback: function importerDataAddCallback(grid, newObjects) {
            $scope.myData = $scope.data.concat(newObjects);
        },


        columnDefs: [

            { field: 'Title' },
            { field: 'Price', enableFiltering: false },
            { field: 'Count'},
            {
                field: 'Do', enableFiltering: false, enableSort: false,
                cellTemplate: '<a href="" ng-click="grid.appScope.openEditDialog(); grid.appScope.getProductById(row.entity)" class="btn btn-default">Edit</a> <a href="" ng-click="grid.appScope.openDeleteDialog(); grid.appScope.getProductById(row.entity)" class="btn btn-default">Delete</a>'
            }

        ],

    };
    $scope.gridOptions.multiSelect = false;
    $scope.gridOptions.modifierKeysToMultiSelect = false;
    $scope.gridOptions.noUnselect = true;
    $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
    };

    $scope.toggleRowSelection = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $scope.gridOptions.enableRowSelection = !$scope.gridOptions.enableRowSelection;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
    };


    function getRowId(row) {

        return row.ProductId;

    }


    //show all products
    loadProducts();
    function loadProducts() {
        var Products = crudservice.getAllProducts();
        Products.then(function (data) {
            $scope.Products = data.data;
            $scope.gridOptions.data = $scope.Products;
            $scope.gridApi.core.refresh();

        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }

    //open create dialog
    $scope.showAlertDetails = function () {
        ngDialog.openConfirm({
            template: 'addProd',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByNavigation: true,
        }).then(function (Product) {
            var saveproduct = crudservice.saveProduct(Product);
            saveproduct.then(function (d) {

                $scope.Product;
                loadProducts();

            });
        });
    }

    //open edit dialog
    $scope.openEditDialog = function () {
        ngDialog.openConfirm({
            template: 'getProd',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByNavigation: true,
        }).then(function (d) {

            var Product = {
            ProductId: $scope.ProductId,
            Title: $scope.Product.Title,
            Price: $scope.Product.Price
            };
            var updaterecords = crudservice.editProduct(Product);
            updaterecords.then(function (d) {
                $scope.Product;
                loadProducts();
            },
                function () {
                    console("Oops..", "Error occured while loading", "error");
                });
        });
    }

    //get by id
    $scope.getProductById = function (Product) {
        var singlerecord = crudservice.getProductId(Product.ProductId);
        singlerecord.then(function (d) {
            var record = d.data;
            $scope.ProductId = record.ProductId;
            $scope.Product.Title = record.Title;
            $scope.Product.Price = record.Price;
        },
            function () {
                console("Oops...", "Error occured while getting record", "error");
            });
    }

    //get statisctic section
    $scope.getSection = function (Product) {
        var singlerecord = crudservice.getProductSection(Product.ProductId);
        singlerecord.then(function (d) {
            var record = d.data;
            $scope.Price = record.Price;
            $scope.Title = record.Title;
            $scope.Count = record.Count;
            $scope.Total = record.Total;
            $scope.Error = '';
        },
            function () {
                $scope.Error = "Данного товара нет на складе!";
                $scope.Price = '';
                $scope.Title = '';
                $scope.Count = '';
                $scope.Total = '';
                console("Oops...", "Error occured while getting record", "error");
            });
    }

    //section dialog
    $scope.openSectionDialog = function () {
        ngDialog.open({
            template: 'getSecion',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    }

    $scope.openDeleteDialog = function () {
        ngDialog.openConfirm({
            template: 'delProd',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope,

            closeByNavigation: true,
        }).then(function (Product) {
            var deleterecord = crudservice.deleteProduct($scope.ProductId);
            deleterecord.then(function (d) {
                $scope.Product;
                loadProducts();
            },
                function () {
                    console("Oops..", "Error occured while loading", "error");
                });
        });
    }

    //close dialog window
    $scope.closeAll = function () {
        ngDialog.closeAll();
    };
});