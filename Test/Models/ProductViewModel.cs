﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Max length - 50 ")]
        [Index(IsUnique = true)]
        public string Title { get; set; }
        [Range(0, 10000, ErrorMessage = "Max value -10000")]
        public float Price { get; set; }
        public int Count { get; set; } = 0;
    }
}