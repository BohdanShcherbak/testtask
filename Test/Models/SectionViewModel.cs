﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class SectionViewModel
    {
        public float Price { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }
        public float Total { get; set; }

    }
}